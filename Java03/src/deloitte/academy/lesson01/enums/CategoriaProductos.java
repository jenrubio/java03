package deloitte.academy.lesson01.enums;

/**
 * Clase del tipo ENUM. Permite desplegar mensajes en la pantalla.
 * Los mensajes describen la clasificación/categoría del producto agregado.
 * @author jenrubio
 *
 */
public enum CategoriaProductos {
	CELULARES, COMPUTO, CAMARAS;
		
}
