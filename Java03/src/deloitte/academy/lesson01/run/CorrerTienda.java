package deloitte.academy.lesson01.run;

import java.util.ArrayList;


import deloitte.academy.lesson01.enums.CategoriaProductos;
import deloitte.academy.lesson01.operation.Productos;
import deloitte.academy.lesson01.operation.TiendaOnline;

/**
 * Clase run: Permite correr los m�todos de la clase TiendaOnline.
 * @author jenrubio
 *
 */

public class CorrerTienda {
	
	public static ArrayList<Productos> listaStock = new ArrayList<Productos>();
	public static ArrayList<Productos> listaVendidos = new ArrayList<Productos>();
	
	/**
	 * Clase main
	 * @param args
	 */
	public static void main(String[] args) {
		Productos productoNuevo = new Productos();
		Productos producto1 = new Productos();
		Productos producto2 = new Productos();
		Productos producto3 = new Productos();
		Productos producto4 = new Productos();
		Productos eliminarP = new Productos();
		TiendaOnline tienda = new TiendaOnline();

		producto1.setCantidad(1);
		producto1.setCodigo(12);
		producto1.setNombre("Camara");
		tienda.agregarProducto(producto1);
		
		producto2.setCantidad(10);
		producto2.setCodigo(10);
		producto2.setNombre("Laptop");
		tienda.agregarProducto(producto2);
		
		producto3.setCantidad(5);
		producto3.setCodigo(11);
		producto3.setNombre("Celular");
		tienda.agregarProducto(producto3);
		
		producto4.setCantidad(22);
		producto4.setCodigo(12);
		producto4.setNombre("Camara");
		tienda.agregarProducto(producto4);
		
		productoNuevo.setCantidad(1);
		productoNuevo.setCodigo(10);
		productoNuevo.setNombre("Laptop");
		tienda.agregarProducto(productoNuevo);

	
		//For each que imprime la lista de stock.
		for (Productos producto : listaStock) {
			if(producto.getCodigo() == 12) {
				System.out.print(CategoriaProductos.CAMARAS +"\n");
			}else if(producto.getCodigo() == 11) {
				System.out.print(CategoriaProductos.CELULARES + "\n");
			}else if(producto.getCodigo() == 10) {
				System.out.print(CategoriaProductos.COMPUTO + "\n");
			}
			System.out.println("Nombre: " + producto.getNombre() + " C�digo: "+ producto.getCodigo() +" Cantidad: "+ producto.getCantidad());
		}
		
		
		eliminarP.setCantidad(7);
		eliminarP.setCodigo(12);
		eliminarP.setNombre("Camara");
		tienda.comprarProducto(eliminarP);
		
		
		//For each para imprimir la lista de los productos vendidos. 
		for (Productos producto : listaVendidos) {
			System.out.println("Vendidos: \nNombre: " + producto.getNombre() + " C�digo: "+ producto.getCodigo() +" Cantidad: "+ producto.getCantidad());
			
			System.out.println("\nLista productos actualizada:");
			
			//For each para imprimir la lista de stock actualizada.
			for (Productos productoz : listaStock) {
				System.out.println("Nombre: " + productoz.getNombre() + " C�digo: "+ productoz.getCodigo() +" Cantidad: "+ productoz.getCantidad());
			}
		}
		
		
		System.out.println("\n\nCantidad de productos vendidos: " + tienda.vendidos);
	}

}
