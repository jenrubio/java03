package deloitte.academy.lesson01.operation;

/**
 * Clase productos. 
 * Contiene getters, setters y constructores.
 * @author jenrubio
 *
 */
public class Productos {
	public int codigo;
	public int cantidad;
	public String nombre;
	
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	public String getNombre() {
		return nombre;
	}

	public Productos() {
		// TODO Auto-generated constructor stub
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public Productos(int codigo, String nombre, int cantidad) {
		super();
		this.cantidad = cantidad;
		this.codigo = codigo;
		this.nombre = nombre;
	}
}
