package deloitte.academy.lesson01.operation;

import java.util.logging.Level;
import java.util.logging.Logger;

import deloitte.academy.lesson01.run.CorrerTienda;

/**
 * Clase que permite agregar o eliminar de stock un producto.
 * @author jenrubio
 *
 */
public class TiendaOnline {

	// Declaraci�n del Logger
	private static final Logger LOGGER = Logger.getLogger(Logger.class.getName());

	public static int vendidos = 0;

	/**
	 * M�todo void que permite agregar un producto a la listaStock
	 * @param producto: Objeto del tipo de la clase Productos.
	 */
	public void agregarProducto(Productos producto) {
		int cantidad = 0;
		
		try {
			if(CorrerTienda.listaStock.isEmpty()) {
				CorrerTienda.listaStock.add(producto);
			}
			else{
				for (Productos productos : CorrerTienda.listaStock) {
					if (productos.getCodigo() == producto.getCodigo()) {
						cantidad = productos.getCantidad() + producto.getCantidad();
						productos.setCantidad(cantidad);
						break;
					} 
					else{
						CorrerTienda.listaStock.add(producto);
						break;
					}
				}
			}
			LOGGER.info("Producto agregado correctamente");
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Ocurri� un error, vuelve a intentarlo", ex);
		}
		

	}

	/**
	 * M�todo void que permite "comprar" o "quitar" de stock un producto que sea
	 * ingresado por el usuario.
	 * @param producto: Objeto de la clase Productos.
	 */
	public void comprarProducto(Productos producto) {
		int cantidad = 0;
		int cantidadV = 0;
		
		try {
			for (Productos productos : CorrerTienda.listaStock) {
				if (productos.getCodigo() == producto.getCodigo()) {
					cantidad = productos.getCantidad() - producto.getCantidad();
					productos.setCantidad(cantidad);
					if(CorrerTienda.listaVendidos.isEmpty()) {
						CorrerTienda.listaVendidos.add(producto);
						break;
					}
					else {
						for (Productos productos2 : CorrerTienda.listaVendidos) {
							if (productos2.getCodigo() == producto.getCodigo()) {
								cantidadV = productos2.getCantidad() + producto.getCantidad();
								productos2.setCantidad(cantidadV);
								break;
							}
						}
						break;
					}
				}
			}
			numVendidos(producto.getCantidad());
			LOGGER.info("Producto eliminado correctamente");
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Ocurri� un error, vuelve a intentarlo", ex);
		}

	}

	/**
	 * M�todo est�tico que no contiene entradas. Se manda a llamar en el m�todo
	 * ComprarProducto.
	 * 
	 * @return vendidos: contador de los productos vendidos (en general).
	 */
	public static int numVendidos(int cantidad) {
		try {
			vendidos += cantidad;
			LOGGER.info("Operaci�n exitosa.");
		}catch(Exception ex) {
				LOGGER.log(Level.SEVERE, "Ocurri� un error", ex);
		}
		
		return vendidos;
	}

}
